package org.zkoss.zkspringmvc.demo.viewConfig;

import org.zkoss.zkspringmvc.demo.model.SidebarPage;

import java.util.List;

/**
 * Author: LamPT
 * Company: Vissoft
 * Create_date: 6/23/2021, 9:31 AM
 * Create with: Intellij IDEA
 */
public interface SideBarPageConfig {
    /** get pages of this application **/
    public List<SidebarPage> getPages();

    /** get page **/
    public SidebarPage getPage(String name);
}
