package org.zkoss.zkspringmvc.demo.viewConfig;



import org.zkoss.zkspringmvc.demo.model.SidebarPage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;


public class SideBarConfig implements SideBarPageConfig{
    HashMap<String, SidebarPage> pageMap = new LinkedHashMap();
    public SideBarConfig(){
        pageMap.put("fn1",new SidebarPage("zk","ZK","/imgs/site.png","http://www.zkoss.org/"));
        pageMap.put("fn5",new SidebarPage("fn5","Simple Table","/imgs/fn.png", "./simple-table.zul"));
    }

    @Override
    public List<SidebarPage> getPages() {
        List<SidebarPage> sideBarList = new ArrayList(pageMap.values());
        return sideBarList;
    }

    @Override
    public SidebarPage getPage(String name) {
        return pageMap.get(name);
    }
}
