package org.zkoss.zkspringmvc.demo.entity;

import java.util.Date;

public class ArrestDetentionInfo {
    private Long id;

    private Integer shareInfoLevel;

    private Long code;

    private String arrestingUnitName;

    private Date procuracyTakenOverDate;

    private String takenOverProcuratorName;

    private Date procuratorAssignmentDate;

    private String arrestContent;

    private String arrestEnactmentName;

    private String lawClauseName;

    private String lawPointName;

    public ArrestDetentionInfo() {
    }

    public ArrestDetentionInfo(Long id, Integer shareInfoLevel, Long code, String arrestingUnitName, Date procuracyTakenOverDate, String takenOverProcuratorName, Date procuratorAssignmentDate, String arrestContent, String arrestEnactmentName, String lawClauseName, String lawPointName) {
        this.id = id;
        this.shareInfoLevel = shareInfoLevel;
        this.code = code;
        this.arrestingUnitName = arrestingUnitName;
        this.procuracyTakenOverDate = procuracyTakenOverDate;
        this.takenOverProcuratorName = takenOverProcuratorName;
        this.procuratorAssignmentDate = procuratorAssignmentDate;
        this.arrestContent = arrestContent;
        this.arrestEnactmentName = arrestEnactmentName;
        this.lawClauseName = lawClauseName;
        this.lawPointName = lawPointName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getShareInfoLevel() {
        return shareInfoLevel;
    }

    public void setShareInfoLevel(Integer shareInfoLevel) {
        this.shareInfoLevel = shareInfoLevel;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public String getArrestingUnitName() {
        return arrestingUnitName;
    }

    public void setArrestingUnitName(String arrestingUnitName) {
        this.arrestingUnitName = arrestingUnitName;
    }

    public Date getProcuracyTakenOverDate() {
        return procuracyTakenOverDate;
    }

    public void setProcuracyTakenOverDate(Date procuracyTakenOverDate) {
        this.procuracyTakenOverDate = procuracyTakenOverDate;
    }

    public String getTakenOverProcuratorName() {
        return takenOverProcuratorName;
    }

    public void setTakenOverProcuratorName(String takenOverProcuratorName) {
        this.takenOverProcuratorName = takenOverProcuratorName;
    }

    public Date getProcuratorAssignmentDate() {
        return procuratorAssignmentDate;
    }

    public void setProcuratorAssignmentDate(Date procuratorAssignmentDate) {
        this.procuratorAssignmentDate = procuratorAssignmentDate;
    }

    public String getArrestContent() {
        return arrestContent;
    }

    public void setArrestContent(String arrestContent) {
        this.arrestContent = arrestContent;
    }

    public String getArrestEnactmentName() {
        return arrestEnactmentName;
    }

    public void setArrestEnactmentName(String arrestEnactmentName) {
        this.arrestEnactmentName = arrestEnactmentName;
    }

    public String getLawClauseName() {
        return lawClauseName;
    }

    public void setLawClauseName(String lawClauseName) {
        this.lawClauseName = lawClauseName;
    }

    public String getLawPointName() {
        return lawPointName;
    }

    public void setLawPointName(String lawPointName) {
        this.lawPointName = lawPointName;
    }
}
