package org.zkoss.zkspringmvc.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zkspringmvc.demo.dao.OracleRepository;
import org.zkoss.zkspringmvc.demo.dao.OracleRepositoryImpl;
import org.zkoss.zkspringmvc.demo.entity.ArrestDetentionInfo;
import org.zkoss.zul.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/mvc/table")
@SessionAttributes("table")
public class SimpleTableController extends SelectorComposer<Component> {

    @Wire
    private Grid simpleTable;

    private OracleRepository repo;
    private List<ArrestDetentionInfo> list;

    public SimpleTableController(){
        repo=new OracleRepositoryImpl();
        list=new ArrayList();
    }

    @RequestMapping(value = "/", method = {RequestMethod.GET,RequestMethod.POST})
    public String showPage(){
        return "mvc/simple-table.zul";
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

        list=repo.getAll();
        Rows rows=simpleTable.getRows();
        if(list.size()>0){
            for(ArrestDetentionInfo data:list){
                Row row=constructRow(data);
                rows.appendChild(row);
            }
        }
    }

    public Row constructRow(ArrestDetentionInfo data){
        SimpleDateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy");

        Row row = new Row();
        row.appendChild(new Label(data.getId().toString()));
        row.appendChild(new Label(data.getShareInfoLevel().toString()));
        row.appendChild(new Label(data.getCode().toString()));
        row.appendChild(new Label(data.getArrestingUnitName()));
        row.appendChild(new Label(data.getProcuracyTakenOverDate()==null?"":dateFormat.format(data.getProcuracyTakenOverDate())));
        row.appendChild(new Label(data.getTakenOverProcuratorName()));
        row.appendChild(new Label(data.getProcuratorAssignmentDate()==null?"":dateFormat.format(data.getProcuratorAssignmentDate())));
        row.appendChild(new Label(data.getArrestContent()));
        row.appendChild(new Label(data.getArrestEnactmentName()));
        row.appendChild(new Label(data.getLawClauseName()));
        row.appendChild(new Label(data.getLawPointName()));
        row.appendChild(new Button("testButton"));

        return row;
    }
}
