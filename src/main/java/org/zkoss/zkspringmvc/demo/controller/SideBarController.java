package org.zkoss.zkspringmvc.demo.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.SerializableEventListener;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zkspringmvc.demo.model.SidebarPage;
import org.zkoss.zkspringmvc.demo.viewConfig.SideBarConfig;
import org.zkoss.zkspringmvc.demo.viewConfig.SideBarPageConfig;
import org.zkoss.zul.*;

@Controller
@RequestMapping("/mvc/sidebar")
@SessionAttributes("demo")
public class SideBarController extends SelectorComposer<Component> {

    @Wire
    private Grid sideBar;

    private SideBarPageConfig pageConfig = new SideBarConfig();

    @RequestMapping(value = "/page", method = {RequestMethod.GET,RequestMethod.POST})
    public String showPage(){
        return "mvc/sidebar.zul";
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception{
        super.doAfterCompose(comp);

        //initialize view after view construction.
        Rows rows = sideBar.getRows();

        for(SidebarPage page:pageConfig.getPages()){
            Row row = constructSidebarRow(page.getLabel(),page.getIconUri(),page.getUri());
            rows.appendChild(row);
        }
    }

    private Row constructSidebarRow(String label, String imageSrc, final String locationUri) {

        //construct component and hierarchy
        Row row = new Row();
        Image image = new Image();
        image.setSrc(imageSrc);
        Label lab = new Label(label);

        row.appendChild(image);
        row.appendChild(lab);

        //set style attribute
        row.setSclass("sidebar-fn");

        //create and register event listener
        EventListener<Event> actionListener = new SerializableEventListener<Event>() {
            private static final long serialVersionUID = 1L;

            public void onEvent(Event event) throws Exception {
                //redirect current url to new location
                if(locationUri.startsWith("http")){
                    Executions.getCurrent().sendRedirect(locationUri);
                }else{
                    Include include = (Include) Selectors.iterable(sideBar.getPage(), "#content")
                            .iterator().next();
                    include.setSrc(locationUri);
                }

            }
        };

        row.addEventListener(Events.ON_CLICK, actionListener);

        return row;
    }
}
