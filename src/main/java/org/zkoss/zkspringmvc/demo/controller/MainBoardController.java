package org.zkoss.zkspringmvc.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Window;

@Controller
@RequestMapping("/mvc/main")
@SessionAttributes("main")
public class MainBoardController extends SelectorComposer<Component> {

//    @Wire
//    private Window mainBoard;

    @RequestMapping(value = "/", method = {RequestMethod.GET,RequestMethod.POST})
    public String showPage(){
        return "mvc/mainboard.zul";
    }
}
