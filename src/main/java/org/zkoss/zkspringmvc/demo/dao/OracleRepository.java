package org.zkoss.zkspringmvc.demo.dao;

import org.zkoss.zkspringmvc.demo.entity.ArrestDetentionInfo;

import java.util.List;

/**
 * Author: LamPT
 * Company: Vissoft
 * Create_date: 6/23/2021, 3:41 PM
 * Create with: Intellij IDEA
 */
public interface OracleRepository {
    List<ArrestDetentionInfo> getAll();
    ArrestDetentionInfo getByCode(Integer code);
}
