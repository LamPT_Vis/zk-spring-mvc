package org.zkoss.zkspringmvc.demo.dao;

import org.zkoss.zkspringmvc.demo.entity.ArrestDetentionInfo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Author: LamPT
 * Company: Vissoft
 * Create_date: 6/23/2021, 4:08 PM
 * Create with: Intellij IDEA
 */
public class OracleRepositoryImpl implements OracleRepository{

    @Override
    public List<ArrestDetentionInfo> getAll() {
        List<ArrestDetentionInfo> detentionInfoList = new ArrayList();
        try {
            String query="SELECT * FROM SPP_REPORT.ARREST_DETENTION_INFO";
            Statement stm = OracleConnector.getConnection().createStatement();

            ResultSet rs = stm.executeQuery(query);
            while (rs.next()){
                ArrestDetentionInfo info = new ArrestDetentionInfo();
                info.setId(rs.getLong(1));
                info.setShareInfoLevel(rs.getInt(2));
                info.setCode(rs.getLong(3));
                info.setArrestingUnitName(rs.getString(5));
                info.setTakenOverProcuratorName(rs.getString(8));
                info.setProcuracyTakenOverDate(rs.getDate(6));
                info.setArrestContent(rs.getString(23));
                info.setArrestEnactmentName(rs.getString(12));
                info.setLawClauseName(rs.getString(14));
                info.setLawPointName(rs.getString(16));

                detentionInfoList.add(info);
            }
            return detentionInfoList;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return detentionInfoList;
    }

    @Override
    public ArrestDetentionInfo getByCode(Integer code) {
        return null;
    }
}
