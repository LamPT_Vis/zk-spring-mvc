package org.zkoss.zkspringmvc.demo.dao;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * Author: LamPT
 * Company: Vissoft
 * Create_date: 6/23/2021, 3:33 PM
 * Create with: Intellij IDEA
 */
public class OracleConnector {
    private static final String DRIVER_NAME="oracle.jdbc.driver.OracleDriver";
    private static final String DATABASE_URI="jdbc:oracle:thin:@45.122.253.178:2151:cdb241";
    private static final String USER_NAME="spp_report";
    private static final String PASSWORD="Ab123456";

    public static Connection getConnection(){
        try{
            Class.forName(DRIVER_NAME);
            Connection con= DriverManager.getConnection(DATABASE_URI,USER_NAME,PASSWORD);
            return con;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
