package org.zkoss.zkspringmvc.demo.model;

import java.io.Serializable;

/**
 * Author: LamPT
 * Company: Vissoft
 * Create_date: 6/23/2021, 9:25 AM
 * Create with: Intellij IDEA
 */
public class SidebarPage implements Serializable {
    private static final long serialVersionUID = 1L;
    String name;
    String label;
    String iconUri;
    String uri;

    public SidebarPage(String name, String label, String iconUri, String uri) {
        this.name = name;
        this.label = label;
        this.iconUri = iconUri;
        this.uri = uri;
    }

    public String getName() {
        return name;
    }

    public String getLabel() {
        return label;
    }

    public String getIconUri() {
        return iconUri;
    }

    public String getUri() {
        return uri;
    }
}
